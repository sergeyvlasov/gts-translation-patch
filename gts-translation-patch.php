<?php
/*
Plugin Name: GTS Translation patch
Plugin URI: http://gts-translation.com/
Description: This plugin patches the origin plugin's bug with broken page links
Author: Sergey Vlasov
Version: 0.1
Author URI: http://sergeyvlasov.com/
*/
error_reporting(E_ALL ^ E_NOTICE);
require_once dirname(__FILE__) . '/sv-util.php';
//////////////////////////////////////////////////////////////////////////////////////////////////

function action_log(){
	//global $wp;
	
	//SVUtil::log($wp);
	SVUtil::trace();
}


//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
$actions = array(
'muplugins_loaded',
'plugins_loaded',
'sanitize_comment_cookies',
'setup_theme',
'load_textdomain',
'after_setup_theme',
'auth_cookie_malformed',
'set_current_user',
'init',
'widgets_init',
//'register_sidebar',
//'wp_register_sidebar_widget',
'wp_loaded',
'parse_request',
'send_headers',
'parse_query',
'pre_get_posts',
'posts_selection',
'wp',
'template_redirect',
'get_header',
'wp_head',
'wp_enqueue_scripts',
'wp_print_styles',
'wp_print_scripts',
'get_template_part_loop',
'loop_start',
'the_post',
'loop_end'
);

class EventHandler {
	
	var $hookName = '';
	
	public function __construct($hookName){
		$this->hookName = $hookName;
	}
	
	public function action($value='')
	{
		SVUtil::trace();
		
	}
	
	public function filter($value='')
	{
		$this->action();
		return $value;
	}
	
	////////////////////////////////////
	function _log_query(){
		global $wp;
		
		SVUtil::log(array(
			"action: " => $this->hookName,
			"query string: " => $wp->query_string,
			"request: " => $wp->request,
			"matched_rule: " => $wp->matched_rule,
			"matched_query: " => $wp->matched_query
		));
		
		
	}
	
	
	function wp_head(){
		$this->_log_query();
	}
	
	var $reversed = array();
	
	function _parse_request($wp){
		//global $wp;
		global $wp_rewrite;
				
		$i=0;
		foreach($wp_rewrite->rules as $rule => $query){
			//SVUtil::log("$i: $rule => $query");	
			$reversed[$rule] = $i;
			$i++;
		}
		//$wp->query_string = "category_name=muestra-pagina&pagename=%2Fsample-page%2Fyellow-page>s_pagepath=%2Fsample-page%2Fyellow-page";
		SVUtil::log(array(
			"action: " => $this->hookName,
			"query string: " => $wp->query_string,
			"request: " => $wp->request,
			"matched_rule: " => $wp->matched_rule,
			"matched rule number: " => $reversed[$wp->matched_rule],
			"matched_query: " => $wp->matched_query
		));
		
	}

	function parse_request($wp){
		/**
		 * [category_name] => muestra-pagina
            [pagename] => /sample-page/yellow-page
            [gts_pagepath] => /sample-page/yellow-page
		 */
		global $wpdb;
		global $wp_rewrite;
		global $gts_plugin;
		
		
		if( //the fix is for this permalink structure only
			$wp_rewrite->permalink_structure == '/%category%/%postname%/' 
			//category_name should be set but nothing else
			&& isset($wp->query_vars['category_name']) 
			&& !isset($wp->query_vars['name']) 
			&& !isset($wp->query_vars['page_name']) 
			&& !isset($wp->query_vars['gts_pagepath'])
			//should be a name without slashes
			&& !preg_match("/\//", $wp->query_vars['category_name'])
			//should not be a real category
			&& !term_exists($wp->query_vars['category_name'])
		  )
		{
			$page_name = $wp->query_vars['category_name'];
			
			//if called before GTS Plugin
			//try to fix it
			if(!($gts_plugin->language)){
				$gts_plugin->update_language_from_wp_query($wp);
			}
			//then continue as usual
			$lang = $gts_plugin->language;
			
			$page_name = SVUtil::orig_page_name($page_name, $lang);
			//SVUtil::log(array("page_name"=>$page_name));
			
			$page_name = "/$page_name";
			//SVUtil::log(array("page_name"=>$page_name));
			
			//$page_name = '/sample-page';
			//SVUtil::log(array("page_name"=>$page_name));
			
			$wp->query_vars = array('pagename' => $page_name);
		}
		
	}
}


foreach ($actions as $action) {
	
	$handler = new EventHandler($action);
	
	
	//add_action($action, array($handler, 'action'));
	switch ($action) {
		case 	'parse_request':
		//case  	'wp_head':
		//case	'send_headers':
		//case	'parse_query':
		//case	'pre_get_posts':
		//case	'posts_selection':
		//case	'wp':
		//case	'template_redirect':
		//case	'get_header':
			add_action($action, array($handler, 'parse_request'));
		break;
	}

}















