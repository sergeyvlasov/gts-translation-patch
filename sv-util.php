<?php


error_reporting(E_ALL ^ E_NOTICE);

class SVUtil{
	
	static function orig_page_name($page_name, $lang){
		global $wpdb;
		
		$select_tran_page = "select * from {$wpdb->prefix}gts_translated_posts where post_slug='{$page_name}' and language='{$lang}'";
		//SVUtil::log(array('$select_tran_page' => $select_tran_page));
		
		$tran_page = $wpdb->get_row($select_tran_page);
		//SVUtil::log(array('$tran_page' => $tran_page));
		
		$orig_id = $tran_page->local_id;
		//SVUtil::log(array('$orig_id' => $orig_id));
		
		$select_orig_page = "select * from {$wpdb->posts} where id={$orig_id}";
		//SVUtil::log(array('$select_orig_page' => $select_orig_page));
		
		$orig_page = $wpdb->get_row($select_orig_page);
		//SVUtil::log(array('$orig_page' => $orig_page));
		
		$result = $orig_page->post_name;
		//SVUtil::log(array('$result' => $result));
		
		return $result;
	}
	
	static function log($msg){
		?>
		<div>
		<pre>
			
			<?php print_r($msg); ?>
		
		</pre>
			
			
		</div>
		<?php
	}
	
	static function trace(){
		$bt = debug_backtrace();
		?>
		<ul'>
		<?php for($i=1; $i<4; $i++): ?>
		<?php $line = $bt[$i]; ?>
			<?php 
			$file = $line['file'];
			$function = $line['function'];
			$linenum = $line['line'];
			$args = $line['args'];
			/**
			 call_user_func_array => /home/user/dev/sites/wpmayor.local/wordpress/wp-includes/plugin.php : 405
			 */
			  ?>
					<li>
						<?php 
							
							if($i==2 && $function == 'call_user_func_array'):
								echo("$i: {$args[0][0]->hookName}"); //
							elseif($i==2 && $function == 'do_action'):
								echo("$i: {$args[0][0]->hookName}"); 
							elseif($i==3):
								echo("file: $file :: $linenum"); //
							endif;
						?>
					</li>
				
		<?php endfor; ?>
		</ul>
		<?php
	}
}

















